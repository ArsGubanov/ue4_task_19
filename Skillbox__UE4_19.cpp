﻿#include <iostream>
using namespace std;

class Animal
{

public:



    virtual void Voice()
    {
        cout << "Sound!\n" << endl;
        
    }
   
};

class Dog : public Animal
{

public:
    void Voice() override
    {
        cout << "Woof!\n";
    }

};

class Cat : public Animal
{

public:
    void Voice() override
    {
        cout << "Meow!\n";
    }

};

class Cow : public Animal
{

public:
    void Voice() override
    {
        cout << "Moooo!\n";
    }

};

class Duck : public Animal
{

public:
    void Voice() override
    {
        cout << "Quack!\n";
    }

};

class Raven : public Animal
{

public:
    void Voice() override
    {
        cout << "Caw! Caw!\n";
    }

};

int main()
{
    int size;
    size = 5;
    Dog* pDog = new Dog;
    Cat* pCat = new Cat;
    Cow* pCow = new Cow;
    Duck* pDuck = new Duck;
    Raven* pRaven = new Raven;
   
    Animal** sounds = new Animal * [size] {pDog, pCat, pCow, pDuck, pRaven};
    for (int i = 0; i < size; i++)
    {
        sounds[i]->Voice();
    }
}
